$(document).ready(function() {
    
    // Banner
    $('.banner').bxSlider({
        auto: true,
        controls: false,
        mode: 'fade',
        pagerCustom: '#pager',
        pause: 5000
    });
    
    // Testimonial
    $('.testimonial').bxSlider({
        auto: true,
        pager: false,
        pause: 5000,
        nextSelector: '#testi_next',
        prevSelector: '#testi_prev',
        nextText: '<i class="fa fa-chevron-right"></i>',
        prevText: '<i class="fa fa-chevron-left"></i>'
    });
    
    // Script for popup gallery
    /*$('.pop_link').hover(function(){
        $(this).find('.pop_link_overlay').fadeIn('fast');        
    });
    $('.pop_link').mouseleave(function(){
        $(this).find('.pop_link_overlay').fadeOut('fast');        
    });
    $(document).on('click', '.pop_link', function() {
        var curPopLink = $(this);
        var clickedLinkImgSrc = $.trim(curPopLink.find('img').attr('src'));   
        $('.popup_wrap img').attr('src', clickedLinkImgSrc);
        $('.body_overlay').fadeIn();
        $('.popup_wrap').fadeIn();
    });
    $(document).on('click', '.body_overlay', function() {
        $('.popup_wrap').hide();    
        $('.body_overlay').hide();
    });
    
    // Popup controls
    var srcArray = [];
    $('.pop_link').each(function(){
        srcArray.push($.trim($(this).find('img').attr('src')));        
    });
    //console.log(srcArray);
    $(document).on('click', '.popup_next', function(e){
        e.preventDefault();
        var totalImageViews = srcArray.length;
        var curPopNav = $(this);
        var curImageView = $.trim(curPopNav.parent().find('img').attr('src'));
        //console.log(curImageView);
        var curImageViewIndex;
        var nextImageView;
        for (i in srcArray){
            if(srcArray[i] == curImageView){
                //console.log('true');
                curImageViewIndex = i;
            }
        }
        if(curImageViewIndex == (totalImageViews - 1)){
            nextImageView = srcArray[0];
        } else {
            nextImageView = srcArray[parseInt(curImageViewIndex) + 1];
        }
        //console.log(nextImageView);
        $('.popup_wrap img').fadeTo(100, 0.1, function(){
            $('.popup_wrap img').attr('src', nextImageView);
            $('.popup_wrap img').fadeTo(200, 1);
        });
    });
    $(document).on('click', '.popup_prev', function(e){
        e.preventDefault();
        var totalImageViews = srcArray.length;
        var curPopNav = $(this);
        var curImageView = $.trim(curPopNav.parent().find('img').attr('src'));     
        var curImageViewIndex;
        var prevImageView;
        for(i in srcArray){
            if(srcArray[i] == curImageView){
                curImageViewIndex = i;
            }
        }
        if(curImageViewIndex == 0){
            prevImageView = srcArray[parseInt(totalImageViews) - 1];
        } else {
            prevImageView = srcArray[parseInt(curImageViewIndex) - 1];
        }
        //console.log(prevImageView);
        $('.popup_wrap img').fadeTo(100, 0.1, function(){
            $('.popup_wrap img').attr('src', prevImageView);
            $('.popup_wrap img').fadeTo(200, 1);
        });
    });*/
    
    // rest_nav dropdown
    $('.nav_rest_trigger').click(function(){
        $('.nav_menu_rest').slideToggle();    
    });
    $(window).resize(function(){
        if($(window).width() >= 768){
            $('.nav_menu_rest').show();
        } else {
            $('.nav_menu_rest').hide();
        }    
    });
    
    $('.sub_slider').bxSlider({
        controls: false,
        pager: false,
        auto: true
    });
    
    
    $('.gal_slider1').bxSlider({
        pager: false   
    });
    $('.gal_slider2').bxSlider({ 
    	pager: false  
    });
    $('.gal_slider3').bxSlider({
        pager: false    
    });
    $('.gal_slider4').bxSlider({
        pager: false  
    });
    $('.gal_slider6').bxSlider({
        pagerCustom: '#gal6_pager'  
    });
    
});




















