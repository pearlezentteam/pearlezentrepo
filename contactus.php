<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pearlezent</title>
    <link rel="icon" href="images/shortcut_ico.png" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
	<link href='https://fonts.googleapis.com/css?family=Archivo+Narrow:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" />
	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
	
	
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.bxslider.js"></script>
	<script src="js/script.js"></script>
	
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script>
        var myCenter = new google.maps.LatLng(10.9639229, 75.888716);

        function initialize() {
            var mapProp = {
                center: myCenter,
                zoom: 9,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("gmap"), mapProp);

            var marker = new google.maps.Marker({
                position: myCenter,
            });

            marker.setMap(map);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	
</head>
    
<body>
    
    <!-- Header -->
    <div class="header_rest">
        <div class="container">
            <div class="col-lg-12">
                <div class="logo_rest">
                    <img src="images/logo.png" alt="Pearlezent" />
                </div>
                <div class="navigation_rest">
                    <ul class="nav_menu_rest">
                        <li><a href="index.html">home</a></li>
                        <li><a href="aboutus.html">about us</a></li>
                        <li><a href="services.html">services</a></li>
                        <li><a href="teampearlezent.html">team pearlezent</a></li>
                        <li><a href="gallery.html">gallery</a></li>
                        <li><a href="#">contact us</a></li>
                    </ul>
                </div>
                <div class="bd_clear"></div>
            </div>
        </div>
        <span class="nav_rest_trigger"><i class="fa fa-bars"></i></span>
    </div>
    <!-- /Header -->
    
    <div class="contact_wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section_head">
                        we would love to hear from you
                    </h3>
                </div>                
            </div>
            <div class="row" style="margin-top: 30px;">
                <div class="col-lg-4 col-md-4 col-sm-4 contact_col text-center">
                    <div class="contact_ico">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="contact_det text-center">
                        Call us on<br />+91 860669889<br />+91 9037917123<br />+91 9846699998<br />+91 9048348348
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 contact_col text-center">
                    <div class="contact_ico">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="contact_det text-center">
                        13/515 k.puram PO <br />676307
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 contact_col text-center">
                    <div class="contact_ico">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="contact_det text-center">
                        Email us at<br /><a href="mailto:hello@pearlezent.com">hello@pearlezent.com</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="map_wrap">
            <div class="map">
                <div id="gmap"></div>
            </div>
            <div class="contact_form_wrap">
                <div class="container">
                    <form id="contact_form" method="post" action="mail/mail.php">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 sm-6">
                                <div class="form_block">
                                    <input type="text" name="name" placeholder="Enter your name" required="required" />
                                </div>
                                <div class="form_block">
                                    <input type="email" name="email" placeholder="Enter your email ID" required="required" />
                                </div>
                                <div class="form_block">
                                    <input type="text" name="mobile" placeholder="Enter your phone number" />   
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 sm-6">
                                <div class="form_block">
                                    <textarea name="message" placeholder="Enter your message here" required="required"></textarea>
                                </div>
                                <div class="form_block">
                                    <input type="submit" name="submit" value="Send" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="footer_logo">
                        <img src="images/logo.png" alt="" />                    
                    </div>
                    <div class="footer_soc">
                        <a href="http://facebook.com/Pearlezentevents" class="footer_soc_fb">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" class="footer_soc_tw">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#" class="footer_soc_yt">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <h4 class="address_hd">Head Office</h4>
                    <p>13/515  k.puram PO - 676307</p>
                    <h4 class="address_hd">Branches</h4>
                    <ul class="branch_list">
                        <li>Tirur</li>
                        <li>Calicut</li>
                        <li>Cochin</li>
                        <li>Kollam</li>
                        <li>Bangalore</li>
                        <li>Chennai</li>
                    </ul>
                    <p class="footer_cont_det">
                        <span>
                            <i class="fa fa-envelope"></i>
                        </span>                        
                        <a href="mailto:hello@pearlezent.com">hello@pearlezent.com</a>
                    </p>
                    <p class="footer_cont_det">
                        <span>
                            <i class="fa fa-link"></i>
                        </span>
                        <a href="index.html">www.pearlezent.com</a>
                    </p>
                </div>
                <div class="col-lg-4 col-md-4 sm-4 text-center">
                    <div class="quick_lnk_inline">
                        <a href="eventmanagement.html">event management</a><br />
                        <a href="weddingmanagement.html">wedding management</a><br />
                        <a href="birthday.html">theme based birthday party</a><br />
                        <a href="celebrity.html">Celebrity Endorsement</a><br />
                        <a href="adfilm.html">Ad Film Production</a><br />
                        <a href="channel.html">Channel Production</a><br />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer_border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="padding-top: 20px;">
                    <span class="copyrt_txt">
                        Copyright &copy; <?php echo date("Y"); ?> Pearlezent. All Rights Reserved.
                    </span>
                    <span class="bodhi_lnk">
                        Powered by <a href="http://www.bodhiinfo.com/"><img src="images/bodhi_link.png" alt="" /></a>
                    </span>
                </div>
            </div>            
        </div>
    </div>
</body>
</html>
